package calculator

import scala.math.sqrt
object Polynomial extends PolynomialInterface {
  def computeDelta(a: Signal[Double], b: Signal[Double],
                   c: Signal[Double]): Signal[Double] = {
    Signal {
      val va = a()
      val vb = b()
      val vc = c()
      vb * vb - 4 * va * vc
    }
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
                       c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      val d = delta()
      if (d < 0) Set()
      else {
        val vb = b()
        val va = a()
        if (d == 0)
          Set[Double](-vb / (2 * va))
        else
          Set[Double]((-vb + sqrt(d)) / (2 * va), (-vb - sqrt(d)) / (2 * va))
      }
    }
  }
}
